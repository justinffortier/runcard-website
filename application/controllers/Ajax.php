<?php
		define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
		if(!IS_AJAX) {
			die('Restricted access');
		}
		$pos = strpos($_SERVER['HTTP_REFERER'],getenv('HTTP_HOST'));
		if($pos===false){
			die('Restricted access');
		}

require_once ("Main_Controller.php");

class Ajax extends Main_Controller {

	/**
	 * Initialize model class and support libraries
	 *
	 * @access  public
	 * @param   void
	 * @return  void
	 */
	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('form_validation');
	}
}