<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Main Controller
 *
 * @category	Application
 * @uses        CI_Controller
 * @author		Justin Fortier
 * @copyright	Copyright 2014, FunkYouCreative LLC, All Rights Reserved
 */

class Main_Controller extends CI_Controller {
    public $views_used = array();
    public $widgets = array();
    public $modals = array();


    public function __construct() {
        parent::__construct();
        $this->_data['controller'] = $this->router->fetch_class();
        $this->_data['main'] = $this;
    }

    function _renderviews($skipViews = array()){
        $controller = $this->router->fetch_class();
        $method = $this->router->fetch_method();
        $parent = $controller;
        $this->_data['controller'] = $controller;
        $this->_data['method'] = $method;
        $this->_data['parent'] = $parent;

        if (!in_array('head', $skipViews)){
            $this->_chooseview(
                $parent . '/' . 'controllers' . '/' . $controller . '/' . 'common' . '/' . 'head.php',
                $parent . '/' . 'core' . '/' . 'common' . '/' . 'head.php'
            );
        }

        if (!in_array('body-start', $skipViews)){
            $this->_chooseview(
                $parent . '/' . 'controllers' . '/' . $controller . '/' . 'common' . '/' . 'body-start.php',
                $parent . '/' . 'core' . '/' . 'common' . '/' . 'body-start.php'
            );
        }

        if (!in_array('header', $skipViews)){
            $this->_chooseview(
                $parent . '/' . 'controllers' . '/' . $controller . '/' . 'common' . '/' . 'header.php',
                $parent . '/' . 'core' . '/' . 'common' . '/' . 'header.php'
            );    
        }
        if (!in_array('wrapper-start', $skipViews)){
            $this->_chooseview(
                $parent . '/' . 'controllers' . '/' . $controller . '/' . 'common' . '/' . 'wrapper-start.php',
                $parent . '/' . 'core' . '/' . 'common' . '/' . 'wrapper-start.php'
            );
        }

        if (!in_array('sidebar', $skipViews)){
            if ($this->session->userdata('login') == 'success'){
                $this->_chooseview(
                        $parent . '/' . 'controllers' . '/' . $controller . '/' . 'common' . '/' . 'sidebar.php',
                        $parent . '/' . 'core' . '/' . 'common' . '/' . 'sidebar.php'
                );
            }
        }

        if (!in_array('content-start', $skipViews)){
            $this->_chooseview(
                $parent . '/' . 'controllers' . '/' . $controller . '/' . 'common' . '/' . 'content-start.php',
                $parent . '/' . 'core' . '/' . 'common' . '/' . 'content-start.php'
            );
        }

        if (!in_array('error', $skipViews)){
            $this->_chooseview(
                $parent . '/' . 'controllers' . '/' . $controller . '/' . 'content' . '/' . $method . '.php',
                'Error/view-error'
            );
        }

        if (!in_array('content-end', $skipViews)){
            $this->_chooseview(
                $parent . '/' . 'controllers' . '/' . $controller . '/' . 'common' . '/' . 'content-end.php',
                $parent . '/' . 'core' . '/' . 'common' . '/' . 'content-end.php'
            );
        }

        if (!in_array('wrapper-end', $skipViews)){
            $this->_chooseview(
                $parent . '/' . 'controllers' . '/' . $controller . '/' . 'common' . '/' . 'wrapper-end.php',
                $parent . '/' . 'core' . '/' . 'common' . '/' . 'wrapper-end.php'
            );
        }

        if (!in_array('footer', $skipViews)){
            $this->_chooseview(
                $parent . '/' . 'controllers' . '/' . $controller . '/' . 'common' . '/' . 'footer.php',
                $parent . '/' . 'core' . '/' . 'common' . '/' . 'footer.php'
            );
        }

        if (!in_array('body-end', $skipViews)){
            $this->_chooseview(
                $parent . '/' . 'controllers' . '/' . $controller . '/' . 'common' . '/' . 'body-end.php',
                $parent . '/' . 'core' . '/' . 'common' . '/' . 'body-end.php'
            );
        }

        $this->_renderpage();
    }

    function _chooseview($view1, $view2) {
        if (file_exists(APPPATH . 'views/' . $view1)) {
            $view = $view1;
        } else {
            $view = $view2;
        }
        $this->_addview($view);
    }

    function _addview($view) {
            $this->views_used[] = $view;
    }

    function _compilepage() {
        foreach ($this->views_used as $view) {
            $output = $this->load->view($view, $this->_data);
        }
    }

    function _renderpage() {
        foreach ($this->views_used as $view) {
            $this->load->view($view, $this->_data);
        }
    }

    function _currentpage($page){
        $url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        if (strpos($url, $page) !== false) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function _loadwidget($widget, $userPath = NULL){
        $controller = $this->router->fetch_class();
        $method = $this->router->fetch_method();
        $parent = $controller;
        if (file_exists(APPPATH . 'views' . '/' . $parent . '/' . 'controllers' . '/' . $controller . '/' . 'widgets' . '/' . $widget . '.php')){
            $this->load->view($parent . '/' . 'controllers' . '/' . $controller . '/' . 'widgets' . '/' . $widget , $this->_data);
            $this->widgets[] = $parent . '/' . 'controllers' . '/' . $controller . '/' . 'widgets' . '/' . $widget;
        }elseif(file_exists(APPPATH . 'views' . '/' . $parent . '/' . 'core' . '/' . 'widgets' . '/' . $widget . '.php')) {
            $this->load->view($parent . '/' . 'core' . '/' . 'widgets' . '/' . $widget, $this->_data);
            $this->widgets[] = $parent . '/' . 'core' . '/' . 'widgets' . '/' . $widget;
        }elseif(file_exists(APPPATH . 'views' . '/' . 'portal' . '/' . 'core' . '/' . 'widgets' . '/' . $widget . '.php')) {
            $this->load->view('portal' . '/' . 'core' . '/' . 'widgets' . '/' . $widget, $this->_data);
            $this->widgets[] = 'portal' . '/' . 'core' . '/' . 'widgets' . '/' . $widget;
        }elseif(!empty($userPath)){
            if(file_exists(APPPATH . 'views' . '/' . $userPath . '.php')){
                $this->load->view($userPath, $this->_data);
                $this->widgets[] = $userPath;
            }else{
                echo 'Incorrect Widget Path: "' . APPPATH . 'views' . '/' . $userPath . '.php"' . '(using $userPath)';
            }
        }else{
            echo 'Incorrect Widget Path: ' . APPPATH . 'views' . '/' . $parent . '/' . 'controllers' . '/' . $controller . '/' . 'widgets' . '/' . $widget . '.php';
        }

    }
    

    function _loadjs($assets = NULL){
        $this->load->helper('directory');
        $coreJs = directory_map('./js/core');
        sort($coreJs);
        $this->_data['core_js'] = $coreJs;
        if (isset($assets)){
            $this->_data['asset_js'] = $assets;
        }
    }

    function _loadcss($assets = NULL){
        $this->load->helper('directory');
        $coreCss = directory_map('./css/core');
        sort($coreCss);
        $this->_data['core_css'] = $coreCss;
        if (isset($assets)){
            $this->_data['asset_css'] = $assets;
        }
    }

    function _print($var){
        echo '<pre>';
        if (is_array($var)){
            print_r($var);
        }else{
            var_dump($var);
        }
        echo '</pre>';
    }

    function _checkpost($postData, $table){
        $fields = $this->db->list_fields($table);
        $checkedFields = array();
        foreach ($postData as $key => $value){
            if (in_array($key, $fields)){
                $checkedFields['post'][$key] = 1;
            }else{
                $checkedFields['post'][$key] = 0;
            }
        }
        foreach ($fields as $field){
            if (array_key_exists($field, $postData)){
                $checkedFields['table'][$field] = 1;
            }else{
                $checkedFields['table'][$field] = 0;
            }
        }
        return $checkedFields;
    }

    function _associativeToNumeric($array, $key){
        $numericArray = array();
        for($a=0; $a<count($array); $a++){
            $numericArray[$a]= $array[$a][$key];
        }
        return $numericArray;
    }

    function _generateFormGroup($fieldId = NULL, $class = NULL, $style = NULL, $arrayData = NULL, $formGroupClass = NULL, $labelClass = NULL, $fieldContainerClass = NULL, $inputAttr){
        $fieldData = $this->db->select()->from('fields')->where('field_id', $fieldId)->or_where('field_key', $fieldId)->get()->result_array();
        $formGroup = '
        <div class="form-group '.$formGroupClass.'">
            <label class="'.$labelClass.'">'.$fieldData[0]['field_label'].': </label>
            <div class="'.$fieldContainerClass.'">'.
                $this->_generateField($fieldId, $class, $style, $arrayData, FALSE, $inputAttr).
            '</div>
        </div>';
        echo $formGroup;
    }

    function _generateField($fieldId = NULL, $class = NULL, $style = NULL, $arrayData = NULL, $render = TRUE, $inputAttr){
        if (!$fieldId){
            echo 'No field ID given';
            return;
        }

        $fieldData = $this->db->select()->from('fields')->where('field_id', $fieldId)->or_where('field_key', $fieldId)->get()->result_array();
        if (count($fieldData) < 1){
            echo 'No field found with this id. Please check your ID';
        }

        if ($fieldData[0]['field_input_type'] == 'select'){
            $html .= '<select name="'.$fieldData[0]['field_name'].'" class="'.$class.'" id="'.$fieldId.'" style="'.$style.'" '.$inputAttr.'>';
            $options = array();
            $options['labels'] = explode(',', $fieldData[0]['field_options_label']);
            $options['values'] = explode(',', $fieldData[0]['field_options_value']);
            if (count($options['labels']) == count($options['values'])){
                if ($fieldData[0]['field_options_value']){
                    $html .= '<option selected disabled>'.$fieldData[0]['field_default_value'].'</option>';
                }
                for($a=0; $a<count($options['labels']); $a++){
                    $selected = '';
                    if ($arrayData[0][$fieldData[0]['field_name']]){
                        if ($options['values'][$a] == $arrayData[0][$fieldData[0]['field_name']]){
                            $selected = 'selected';
                        }
                    }
                    $html .= '<option '.$selected.' value="'.$options['values'][$a].'">'.$options['labels'][$a].'</option>';
                }
                $html .= '</select>';
            }else{
                $html = 'Label and Value counts do not match';
            }
        }elseif($fieldData[0]['field_input_type'] == 'text'){
            $value = $fieldData[0]['field_default_value'];
            if ($arrayData[0][$fieldData[0]['field_name']]){
                $value = $arrayData[0][$fieldData[0]['field_name']];
            }
            $html .= '<input class="'.$class.'" style="'.$style.'" name="'.$fieldData[0]['field_name'].'" type="'.$fieldData[0]['field_input_type'].'" id="'.$fieldData[0]['field_key'].'" value="'.$value.'" placeholder="'.$fieldData[0]['field_placeholder'].'" ' . $inputAttr.'>';
            $html .= '<i class="ion ion-ios-close-outline clearField selectable" onclick="ClearTextField(this)" style="position: absolute;right: 25px;top: 9px;"></i></input>';
        }elseif($fieldData[0]['field_input_type'] == 'checkbox'){
            $value = $fieldData[0]['field_default_value'];
            if ($value == 1){
                $checked = 'checked';
            }
            $html .= '<input class="'.$class.'" style="'.$style.'" name="'.$fieldData[0]['field_name'].'" type="'.$fieldData[0]['field_input_type'].'" id="'.$fieldData[0]['field_key'].'" value="'.$value.'" placeholder="'.$fieldData[0]['field_placeholder'].'" ' . $inputAttr.' '.$checked.' >';
        }
        if($fieldData[0]['field_required'] == 1){
            $html .= '<i class="ion ion-ios-star red-text" style="position: absolute;right: 0;top: 8px;font-size: 10px;"></i>';
        }

        if ($render){
            echo $html;    
        }else{
            return $html;
        }
    }

    function _referentialToColumn($array){
        if (!is_numeric(array_keys($array)[0])){
            return 'Non-Numeric array requested'; 
            die;
        }

        $returnArray = array();
        for ($a=0; $a<count($array) ; $a++) { 
            foreach($array[$a] as $key => $value){
                $returnArray[$key][] = $array[$a][$key];
            }
        }
        return $returnArray;
    }

    function _checkLinkHttp($urlString){
        $url = parse_url($urlString);
        if($url['scheme'] == 'https' || $url['scheme'] == 'http'){
           $link = $urlString;
        }else{
            $link = 'http://'.$urlString;
        }
        return $link;
    }
}

