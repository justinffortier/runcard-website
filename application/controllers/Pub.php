<?php

/**
 * FYC Igniter Pub Controller
 *
 * @category    Application
 * @uses        Publico
 * @author      Justin Fortier
 * @copyright   Copyright 2014, FunkYouCreative LLC, All Rights Reserved
 */

require_once ("Main_Controller.php");

class Pub extends Main_controller {
	function __construct(){
		parent::__construct(); 
	}
	 function index(){
		$this->_loadcss();
		$this->_loadjs();
		$this->_renderviews();
	}
	 function Company(){
		$this->_loadcss();
		$this->_loadjs();
		$this->_renderviews();
	}
	 function Products(){
		$this->_loadcss();
		$this->_loadjs();
		$this->_renderviews();
	}
	 function Services(){
		$this->_loadcss();
		$this->_loadjs();
		$this->_renderviews();
	}
	 function Clients(){
		$this->_loadcss();
		$this->_loadjs();
		$this->_renderviews();
	}
}
