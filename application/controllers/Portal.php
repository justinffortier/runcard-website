<?php

/**
 * FYC Igniter Pub Controller
 *
 * @category    Application
 * @uses        Publico
 * @author      Justin Fortier
 * @copyright   Copyright 2014, FunkYouCreative LLC, All Rights Reserved
 */

require_once ("Publico.php");

class Pub extends Publico {
	function __construct(){
		parent::__construct(); 
	}
	
	function index(){
		$this->_loadcss();
		$this->_loadjs();
		$this->_renderviews();
	}
}