<div id="QuoteRow" class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<blockquote>
				Using IntraRatio, we were able to set and enforce custom policies and immediately begin
				blocking unwanted traffic—spam and DDoS attacks—with little or no false positives. This
				also eliminated threat actors’ attempts to gather information via scanning.
			</blockquote>
			<div class="QuoteAuthor">
				<span style="color:#19A589;">Lorem Ipsum</span><br>
				<em>Site Director, Meta Solutions - Athens</em>
			</div>
			<div class="quoteButton">
				<a href="<?=base_url()?>products" class="btn">READY TO GET STARTED?</a>
			</div>
		</div>
	</div>
</div>