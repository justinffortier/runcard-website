<div id="IndustriesContainer" class="container-fluid">
	<h2 class="text-center">Our Industries</h2>
	<small class="text-center">
		<p>Church-key pour-over mixtape XOXO iPhone, salvia readymade flexitarian farm-to-table stumptown chia butcher kinfolk.
		Godard XOXO forage, portland gentrify mixtape chicharrones iPhone celiac ugh listicle trust fund readymade. 
		</p>
	</small>
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<img class="img-responsive" src="<?=base_url()?>css/images/company/1electronicslogo.png">
				<small class="text-center"><strong>ELECTRONICS</strong></small>
			</div>
			<div class="col-lg-3">
				<img class="img-responsive" src="<?=base_url()?>css/images/company/2aerospacedefenselogo.png">
				<small class="text-center"><strong>AEROSPACE &<br> DEFENSE</strong></small>
			</div>
			<div class="col-lg-3">
				<img class="img-responsive" src="<?=base_url()?>css/images/company/3semiconductlogo.png">
				<small class="text-center"><strong>SEMICONDUCTORS</strong></small>
			</div>
			<div class="col-lg-3">
				<img class="img-responsive" src="<?=base_url()?>css/images/company/4medicallogo.png">
				<small class="text-center"><strong>MEDICAL<br> DEVICES</strong></small>
			</div>
		</div>
	</div>
</div>