<div id="containerStatements" class="container">
	<div class="row">
		<div class="col-lg-6">
			<h2>Our Company</h2>
				<div class="blueUnderline"></div>
			<div class="CompanyStatements">
				<p>IntraRatio delivers a manufacturing execution system
				   and data analytics solution that improves operational
				   efficiencies through continuous improvement, control,
				   and scalability We ensure that our clients are fully able
				   to utilize our services, to create a faster, more efficient,
				   operation at an affordable cost..</p>
			</div>
		</div>
		<div class="col-lg-6">
			<h2>Our Mission & Values</h2>
			<div class="blueUnderline"></div>
			<div class="CompanyStatements">
				<p>At IntraRatio, we offer two affordable systems to help
				manage your production data: RUNCARD and DATACARD.
				With Industry 4.0, the manufacturing sector will continue
				to see rapid growth and change through big data, analytics,
				and automation. Our two solutions offer data visualization
				and rapid access to information, to help you analyze
				operations, manage production, and ultimately make
				better decisions.</p>
			</div>
		</div>
	</div>
</div>