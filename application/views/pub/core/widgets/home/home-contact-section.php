<div id="contactContainer">
    <div class="ContactFooterHeader"></div>
    <div class="row">
        <div class="col-sm-12">
            <h1 class="text-center">Contact Us Today For A Demo</h1>
            <div class="space"></div>
        </div>
        <div id="contact" class="contactForm">
            <form>
                <input class="form-control" placeholder="Name"> 
                <input class="form-control" placeholder="Email"> 
                <textarea class="form-control" placeholder="Message"></textarea>
                <button class="btn btn-primary-outline">Send Message</button>
            </form>
        </div>
    </div>
</div>