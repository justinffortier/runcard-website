<div id="homeHeader" class="intro-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="intro-message">
                    <h1>Manufacturing Data <br>Intelligence</h1>
                    <h3>Intelligent manufacturing automation that<br> adapts and scales to customer needs.</h3>
                </div>
            </div>
        </div>
    </div>
</div>