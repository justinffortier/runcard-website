<div id="expertiseContainer" class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <h1 class="text-center">Industry Expertise</h1>
            <div class="space"></div>
        </div>
    </div>
    <div class="container">
        <div id="expertiseLogos" class="row">
            <div class="col-sm-3">
                <img src="<?=base_url()?>css/images/home/electronicslogo.png">
                <h4 class="text-center" style="margin: 0;">Electronics</h4>
            </div>
            <div class="col-sm-3">
                <img src="<?=base_url()?>css/images/home/aerospacelogo.png">
                <h4 class="text-center" style="margin: 0;">Aerospace & Defense</h4>
            </div>
            <div class="col-sm-3">
                <img src="<?=base_url()?>css/images/home/semiconductlogo.png">
                <h4 class="text-center" style="margin: 0;">Semiconductor</h4>
            </div>
            <div class="col-sm-3">
                <img src="<?=base_url()?>css/images/home/meddeviceslogo.png">
                <h4 class="text-center" style="margin: 0;">Medical Devices</h4>
            </div>
        </div>
    </div>
</div>