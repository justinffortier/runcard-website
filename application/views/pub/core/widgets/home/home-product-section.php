<div id="productContainer" class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <h1 class="text-center">Products</h1>
            <div class="space"></div>
        </div>
    </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="productBox">
                    <div class="productImg">
                        <img class="img-responsive" src="<?=base_url()?>css/images/home/productruncard.png">
                    </div>
                    <div class="productCaption">
                        <small>What is RUNCARD?</small>
                    </div>
                    <div class="productInfo">
                        <p>RUNCARD is a low cost ownership system that offers automation, performance management and tracking, operational control and visibility, product tracking and traceability</p>
                    </div>
                    <a href="<?=base_url()?>products/#runcard" class="btn btn-primary">Learn More</a>
                </div>
            </div>
        <div class="col-sm-6">
            <div class="productBox">
                <div class="productImg"><img class="img-responsive" src="<?=base_url()?>css/images/home/productdatacard.png"></div>
                <div class="productCaption"><small>What is DATACARD?</small></div>
                <div class="productInfo"><p>DATACARD offers a powerful, intuitive, real-time yield management solution, creating comprehensive reports from data sets collected from multiple sources, including machine systems and IoT appliances</p></div>
                <a href="<?=base_url()?>products/#datacard" class="btn btn-primary">Learn More</a>
            </div>
        </div>
    </div>
</div>