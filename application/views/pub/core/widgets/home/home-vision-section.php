<div id="MsContainer" class="container-fluid">
    <div class="row">
        <div class="col-sm-6 MsText">
            <h1>Mission Statement & Vision </h1>
            <div class="space"></div>
            <p>At IntraRatio, we offer two affordable systems to help manage your production data: RUNCARD and DATACARD. With industry 4.0, the manufacturing sector will continue to see rapid growth and change through big data, analytics, and automation.</p><br>
            <p>Our two solutions offer data visualization and rapid access to information, to help you analyze operations, manage production, and ultimately make better decisions.</p>
        </div>
        <div class="col-sm-6">
            <div class="laptopMission">
                <img class="img-responsive" src="<?=base_url()?>css/images/home/laptopMission.png">
            </div>
        </div>
    </div>
</div>