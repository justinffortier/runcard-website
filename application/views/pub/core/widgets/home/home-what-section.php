<div id="whatContainer" class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
                <h1 class="text-center">What We Do</h1>
                <div class="space"></div>
                <p class="text-center">IntraRatio delivers a manufacturing execution sytem and data analytics solution that <br> improves operational efficiencies through continuous improvement, control, and scalability.</p>
                <p class="text-center">We ensure that our clients are fully able to utilize our services, to create faster, more <br>efficient, operation at an affordable cost.</p>
        </div>
    </div>
</div>