<div id="customersContainer" class="container-fluid">
<h2 class="text-center">Our Customers</h2>
<div class="space"></div>
	<div class="row">
		<div class="col-sm-4 borderBottom borderRight"><img src="<?=base_url();?>css/images/customers/creditkarma.png" class="img-responsive"></div>
		<div class="col-sm-4 borderBottom borderRight"><img src="<?=base_url();?>css/images/customers/cnnlogo.png" class="img-responsive"></div>
		<div class="col-sm-4 borderBottom"><img src="<?=base_url();?>css/images/customers/fastcompany.png" class="img-responsive"></div>
	</div>
	<div class="row">
		<div class="col-sm-4 borderRight"><img src="<?=base_url();?>css/images/customers/forbeslogo.jpg" class="img-responsive"></div>
		<div class="col-sm-4 borderRight"><img src="<?=base_url();?>css/images/customers/inclogo.jpg" class="img-responsive"></div>
		<div class="col-sm-4"><img src="<?=base_url();?>css/images/customers/techcrunchlogo.png" class="img-responsive"></div>
	</div>
</div>