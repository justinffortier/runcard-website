<div id="products-footer-header" class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="products-foot-message">
                <h4>Need a custom solution, still have questions, or want to leave us feedback?</h4>
                <small class="text-center">We’d love to hear from you!<br> Contact us and we’ll reply quicker than you can say semiconductor</small><br>
                <div class="products-foot-button">
                    <a href="<?=base_url()?>#contact" class="btn">Send Message</a>
                </div>
            </div>
        </div>
    </div>
</div>
