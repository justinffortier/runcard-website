<div id="runcardContainer" class="container-fluid">
    <div id="runcard" class="row">
        <div class="col-lg-6 col-sm-6 col-xs-12">
            <div class="RunProductLogo">
                <img class="img-responsive" src="<?=base_url()?>css/images/products/runcardlogoblue.png">
            </div>
            <div class="RunProductDesc">
                <p>RUNCARD is a low cost of ownership system that offers automation, performance management and tracking, operational control and visibility, product tracking and traceability. With its automated data capture capabilities, RUNCARD reduces the burden of manual data entry, creating a more efficient allocation of resources. RUNCARD allows for higher traceability and root cause analysis, enabling a better understanding of your internal supply chain. In addition, RUNCARD allows you to better manage your production assembly and inventory through integration with other factory systems, making tracking and traceability simple. Through improved data collection and in-depth analysis, it will drive better decision making, leading to a healthier bottom line.
                </p>
                <button class="btn darkBlueBtn">Get Started</button>
            </div>
        </div>
        <div class="col-lg-6 col-sm-6 customPosition hidden-xs"><img class="img-responsive" src="<?=base_url()?>css/images/products/macmag.png"></div>
        <hr>
    </div>
</div>