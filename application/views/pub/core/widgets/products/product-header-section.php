<div id="productHeader" class="intro-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="products-intro-message">
                    <h4>Manage<br>It All!</h4>
                    <div class="quote"><p>Whether it be managing your data or tracking it - a software that can do it all will save the day. Built for efficiency, continued improvement, control, and scalability.</p> </div>
                </div>
            </div>
        </div>
    </div>
</div>
