<div id="datacardContainer" class="container-fluid">
    <div id="datacard" class="row">
        <div class="col-lg-6 col-sm-6 hidden-xs"><img class="img-responsive" src="<?=base_url()?>css/images/products/screenshotleft.png"></div>
        <div class="col-lg-6 col-sm-6 col-xs-12">
            <div class="DataProductLogo">
                <img class="img-responsive" src="<?=base_url()?>css/images/products/datacardlogoblue.png">
            </div>
             <div class="DataProductDesc">
                <p>DATACARD offers a powerful, intuitive, real-time yield management solution, creating comprehensive reports from data sets collected from multiple sources, including machine systems and IoT appliances. Through a comprehensive set of data analysis options, DATACARD enables quick identification of root cause and areas for improvement.  DATACARD natively produces yield trends, failure analysis, statistical process control, and other analytical models. In addition, the ease of integration with other computing tools, such as MATLAB and LabView, helps users utilize existing tools to create automated analysis workflows, for full scalability.
                </p>
                <button class="btn darkBlueBtn">Contact Us Today</button>
            </div>
        </div>
    </div>
</div>