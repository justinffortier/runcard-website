<div id="productContainer" class="container-fluid">
    <div class="row">
        <div class="col-sm-6">
            <div class="productBoxShadow">
                <div class="productImg">
                    <img class="img-responsive" src="<?=base_url()?>css/images/home/productruncard.png">
                </div>
                <div class="productFeature">
                    <small>Features</small>
                </div>
                <div class="productList">
                    <ul>
                        <li>Continuous improvement & efficiency</li>
                        <li>Inventory management</li>
                        <li>Operational efficiency</li>
                        <li>Quality control</li>
                        <li>Workflow management</li>
                        <li>Integration with enterprise platforms<br>
                            <span style="padding-left:5px;">–  ERP/MRP/accounting system WIP tracking Eliminate errors</span></li>
                        <li>Rapid ROI</li>
                        <li>Low cost of ownership, minimized re-training costs and risks</li>
                        <li>Eliminating paper work</li>
                        <li>Barcode & custom label support</li>
                    </ul>
                </div>
                <div class="learnMore"><a href="<?=base_url()?>products/#runcard">Learn More >></a></div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="productBoxShadow classWithPad">
                <div class="productImg"><img class="img-responsive" src="<?=base_url()?>css/images/home/productdatacard.png"></div>
                <div class="productFeature"><small>Features</small></div>
                <div class="productList">
                    <ul>
                        <li>Yield management solution</li>
                        <li>Real time data analysis</li>
                        <li>Automated outlier detection & removal</li>
                        <li>Root cause analysis</li>
                        <li>What-if analysis</li>
                        <li>Scalability</li>
                        <li>Quality control testing</li>
                        <li>Reduced test time</li>
                        <li>Directly integrates with other software, such as JMP, </li>
                        <li>MATLAB, LabVIEW, and Minitab</li>
                        <li>Rapid ROI</li>
                    </ul>
                </div>
                <div class="learnMore"><a href="<?=base_url()?>products/#datacard">Learn More >></a></div>
            </div>
        </div>
    </div>
</div>