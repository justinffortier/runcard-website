<div id="ServicesContact" class="container-fluid">
    <div class="ServicesContactContainer">
        <div id="quote" class="row">
            <div class="col-lg-6 col-sm-6">
                <h2>Ready for a quote?</h2>
                <p>Sartorial cardigan trust fund, marfa post-ironic selfies 3 wolf moon. Tacos quinoa letterpress, green juice beard iPhone fingerstache slow-carb lomo williamsburg. Organic godard try-hard literally, artisan mustache slow-carb helvetica raw </p>
                <h2>More Product Info?</h2>
                <p>Sartorial cardigan trust fund, marfa post-ironic selfies 3 wolf moon.</p>
                <div class="ProductLogosFooter">
                    <img src="<?=base_url()?>css/images/services/datacard-whitelogo.png">
                    <img src="<?=base_url()?>css/images/services/runcard-whitelogo.png">
                </div>
            </div>
            <div class="col-lg-6 col-sm-6">
                <form id="ContactQuote">
                    <input class="form-control" placeholder="Name"> 
                    <input class="form-control" placeholder="Email"> 
                    <textarea class="form-control" placeholder="Message"></textarea>
                    <button class="btn btn-block">Request A Quote</button>
                </form>
            </div>
        </div>
    </div>
</div>