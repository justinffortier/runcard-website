<div id="servicesHeader" class="intro-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="services-intro-message">
                    <h4>MORE THAN<br>JUST IMPLEMENTING!</h4>
                    <div class="quote">Our services go beyond just installation. We make sure our customers have personalized on going support <br>for the success of their business.</div>
                </div>
            </div>
        </div>
    </div>
</div>