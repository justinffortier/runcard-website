<div id="Services" class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <span class="title pull-left">Training <div class="trainingLine"></div></span> 
                <div class="pull-right">
                    <a id="blueButton" href="<?=base_url()?>services/#quote">Request a Quote</a>
                </div>
            </div>
        </div>
        <div class="row TrainingRow">
            <div class="col-lg-6">
                Catered to client specifications<br>
                Focused on efficient utilization to maximize our clients competi-<br>tive advantages<br>
                Providing support and creating a relationship beyond just the solution for our clients
            </div>
            <div class="col-lg-6">
                    Included in training:<br>
                    Documentation<br>
                    2 day hands on interactive on-site training<br>
                    Online help<br>
                    Continuous collaboration to improve solutions
            </div>
        </div>
</div>