<div id="Services" class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <span class="title pull-left">Implementation<div class="implementationLine"></div></span> 
                <div class="pull-right">
                    <a id="blueButton" href="<?=base_url()?>services/#quote">Request a Quote</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="row deploymentProcess">
                    <div class="col-sm-2"><img src="<?=base_url()?>css/images/services/deploymentlogo.png"></div>
                    <div class="col-sm-10">
                        <strong>Deployment process:</strong><br>
                        <em>Understanding specific client needs<br>
                        Testing to make sure we are the right fit for you<br>
                        Training program<br>
                        - creating a relationship with client</em>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="row intergrationProcess">
                <div class="col-sm-2"><img src="<?=base_url()?>css/images/services/intergrationlogo.png"></div>
                <div class="col-sm-10">
                    <strong>Intergration process:</strong><br>
                    <em>Communicate seamlessly between systems and processes<br>
                    Rapid response and execution<br>
                    Continuous collaboration with client to improve operationsNetworking, data mapping/augmentation, performance testsInstall control systems to monitor and ensure solutions run properly</em>
                    </div>
                </div>
            </div>
        </div>
    <hr>
</div>