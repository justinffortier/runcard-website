<footer>
	<?php 
        if ($this->input->get('data') == '1'){
            $main->_print($this->_data);
        }
    ?>
    <?php 
        if ($this->input->get('profiler') == '1'){
            $this->output->enable_profiler(TRUE);
        }
    ?>
    <?php if ($_GET['print']){?>
        <script>
            window.print();;
        </script>
    <?php } ?>
	<?php 
	    if (isset($asset_js) && !empty($asset_js))
            foreach ($asset_js as $js){
                echo '<script src="' . base_url() . 'js/assets/' . $js . '.js"></script>';
            }
    ?>
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-xs-12">
        <div class="row">
          <div class="col-sm-6">
            <ul>
              <li><a href="<?=base_url()?>company">Company</a></li>
              <li><a href="<?=base_url()?>products">Products</a></li>
              <li><a href="<?=base_url()?>services">Services & Support</a></li>
              <li><a href="<?=base_url()?>clients">Clients</a></li>
            </ul>
          </div>
          <div class="col-sm-6">
            <div class="row">
              <div class="col-sm-6 col-xs-6">
                <div class="addressFooter">
                  <p>IntraRatio Corporation</p>
                  <p>151 14th St</p>
                  <p>San Diego, CA 92101</p>
                </div>
              </div>
              <div class="col-sm-6 col-xs-6">
                <div class="contactInfo">
                  <p><i aria-hidden="true" class="fa-lg ion-social-linkedin"></i></p>
                  <p>(858) 324-1051</p>
                  <p>solutions@intraratio.com</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <div class="row">
    <div class="col-sm-12 copyright">
      2016 IntraRatio Corporation. All Rights Reserved.
    </div>
  </div>
</div>
</footer>
