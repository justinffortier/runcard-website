<?php header("Cache-Control: public, max-age=2592000",header("Pragma: cache")); //30days (60sec * 60min * 24hours * 30days)?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="icon" type="x-icon" href=""/>
        <meta charset="UTF-8">
        <title>Runcard Systems</title>
        <meta name="description" content="">
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'/>
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="mobile-web-app-capable" content="yes">
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <?php
            if (isset($core_css)){
                foreach ($core_css as $css){
                    echo '<link href="' . base_url() . 'css/core/' . $css . '" rel="stylesheet" type="text/css" />';
                }
            }
        ?>

        <?php
            if (isset($asset_css)){
                foreach ($asset_css as $css){
                    echo '<link href="' . base_url() . 'css/assets/' . $css . '.css" rel="stylesheet" type="text/css" />';
                }
            }
        ?>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery/jquery-ui.min.js"></script>
        <script>
            site_url = "<?php echo site_url(); ?>";
            base_url = "<?php echo base_url(); ?>";
            controller = "<?php echo $controller ?>";
        </script>
        <?php
            foreach ($core_js as $js){
                echo '<script src="' . base_url() . 'js/core/' . $js . '"></script>';
            }
        ?>
    </head>
