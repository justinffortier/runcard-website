<div class="content">
	<div id="ajaxAlert" class="alert hidden">
		<span onclick="$('#ajaxAlert').addClass('hidden')" class="close">×</span>
		<h4><i id="ajaxIcon" class="ion ionLg"></i> Alerta</h4>
		<hr>
		<span id="ajaxMessage"></span>
	</div>