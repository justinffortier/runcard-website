

<div class="row">
	<div class="col-md-12 col-sm-12">
		<div class="box">
			<div class="box-title">
				<h2 class="box-heading">UNABLE TO LOAD CONTENT PAGE</h2>
			</div>
			<div class="box-body">
				<?php echo APPPATH . 'views/' . $parent . '/' . 'controllers' . '/' . $controller . '/' . 'content' . '/' . $method . '.php'; ?>
			</div>
		</div>
	</div>
</div>