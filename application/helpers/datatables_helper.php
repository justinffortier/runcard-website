<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('test_method'))
{

    function check_percent($percent){
        if($percent < 0)
        	return '<span class="percent-negative">('.number_format($percent, 2).'%) <i class="fa fa-arrow-down"></i></span>';
        else
        	return '<span class="percent-positive">('.number_format($percent, 2).'%) <i class="fa fa-arrow-up"></i></span>';
    }

    function parse_percent($percent){
        if($percent < 0)
        	return '<span class="percent-negative">('.number_format($percent, 2).'%) <i class="fa fa-arrow-down"></i></span>';
        else
        	return '<span class="percent-positive">('.number_format($percent, 2).'%) <i class="fa fa-arrow-up"></i></span>';
    }

	function rank_inc(){
	    global $a;
	    $a++;
	    return $a;
	}
}